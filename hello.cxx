#include <iostream>

/* here is and example of "wrong" format - clang-format will move this curly
 * bracket into same line as main(): "int main() {" */
int main() {
  std::cout << "Hello, World!";
  return 0;
}
