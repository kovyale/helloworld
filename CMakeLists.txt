cmake_minimum_required (VERSION 2.8.11)
set (CMAKE_C_COMPILER "/usr/bin/clang")
set (CMAKE_CXX_COMPILER "/usr/bin/clang++")
set (CMAKE_AR "/usr/bin/llvm-ar")
set (CMAKE_LINKER "/usr/bin/llvm-ld")
set (CMAKE_NM "/usr/bin/llvm-nm")
set (CMAKE_OBJDUMP "/usr/bin/llvm-objdump")
set (CMAKE_RANLIB "/usr/bin/llvm-ranlib")
project (HELLOWORLD)
file(GLOB_RECURSE ALL_SOURCE_FILES *.cxx)
add_custom_target(
        clangformat
        COMMAND clang-format
        -style=LLVM
        -i
        ${ALL_SOURCE_FILES}
)
add_executable (helloWorld hello.cxx)
