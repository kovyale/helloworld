#!/bin/sh
set -e
make clangformat
if [ -n "$(git status | grep modified)" ]; then 
  echo "Running git diff to check formatting errors fixed"
  git diff
  exit 1
fi
